# ska-sdp-daliuge-graphs



## About

This repository is intended to contain a number of EAGLE palettes and DALiuGE graphs for performing a variety of SKA-related data processing workflows.



## Add your files

Ideally, EAGLE (Editor for the Advanced Graph Language Environment, http://eagle.icrar.org) would be used to load, modify and save files within this repository. However, since all development on ska-telescope repositories must be done in branches, the master branch of this repo can only be used to load (read) palettes and graphs.

If a user wishes to modify the graphs contained within this repository, they can use the following steps:

* The master branch of this repository has been added to the default list of repositories in EAGLE, so users can load graphs or palettes as appropriate
* In order to save their changes, a new branch must be created. This can be done with the GitLab web interface, or via the command line.
* Add the newly created branch to EAGLE, using the "Add New Repository" button in the "Translation" tab on the right.
* Save the graph to the branch from EAGLE using the "Git Storage > Save Graph As" item within the "Graph" menu item in the menu bar.

Modifications in branches can then be merged into master using the MR process consistent with other ska-telescope repositories.
