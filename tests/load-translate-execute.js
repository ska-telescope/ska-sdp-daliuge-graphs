import {Selector, ClientFunction} from 'testcafe';
import {networkInterfaces} from 'os';
import http from 'http';
import https from 'https';
import fs from 'fs';

/*
    run with:

    testcafe chrome tests/load-translate-execute.js
*/

let GRAPHS = [];
let REPRO_LOC = "./";

let EAGLE_PORT = 8888;
let EAGLE_IP = "127.0.0.1";

// TODO: determine this using docker inspect, instead of hard-coding
let DALIUGE_ENGINE_IP = "172.18.0.3";
let DALIUGE_TRANSLATOR_IP = "172.18.0.4";

const DALIUGE_DIM_PORT = 8001;
const DALIUGE_NM_PORT = 8000;
const DALIUGE_NODE_PORT = 9000;
const DALIUGE_NODE_START_URL = "/managers/island/start";

const DALIUGE_TRANSLATOR_PORT = 8084;
const DALIUGE_TRANSLATOR_URL = "/gen_pgt";

const SUCCESS_MESSAGE = "Finished";

const PAGE_TRANSITION_WAIT_DURATION = 20000;
const SHORT_WAIT_DURATION = 5000;

const TIMEOUTS_OPTIONS = {
    pageLoadTimeout: 10000,
    pageRequestTimeout: 60000,
    ajaxRequestTimeout: 60000,
};

let graphJSON = "";
let graphReprodata = "";

const startLocalManagers = () => {
    return new Promise((resolve, reject) => {

        const data = JSON.stringify({
            nodes: [DALIUGE_ENGINE_IP]
        });

        const options = {
            hostname: DALIUGE_ENGINE_IP,
            port: DALIUGE_NODE_PORT,
            path: DALIUGE_NODE_START_URL,
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Content-Length': data.length
            }
        };

        const req = http.request(options, res => {
            if (res.statusCode !== 200) {
                console.log(`statusCode: ${res.statusCode}`);
            }

            res.on('data', d => {
                //process.stdout.write(d);
            });

            res.on('end', () => {
                resolve();
            });
        });

        req.on('error', error => {
            console.error(error);
            reject(new Error(error));
        });

        req.write(data);
        req.end();
    });
}

const loadGraph = (path) => {
    return new Promise((resolve, reject) => {
        fs.readFile(path, 'utf8', (err, data) => {
            if (err) {
                reject(err);
                return;
            }
            graphJSON = data;
            resolve();
        });
    });
}

const loadReprodata = (path) => {
    return new Promise((resolve, reject) => {
        fs.readFile(path, 'utf8', (err, data) => {
            if (err) {
                reject(err);
                return;
            }
            console.log("Attempting to parse reprodata JSON");
            graphReprodata = JSON.parse(data);
            console.log("Parsed reprodata JSON");
            resolve();
        });
    });
}

const fetchReprodata = (url) => {
    return new Promise(resolve => {

        http.get(url, res => {
            const {statusCode} = res;
            const contentType = res.headers['content-type'];

            res.setEncoding('utf8');
            let rawData = '';
            res.on('data', (chunk) => {
                rawData += chunk;
            });
            res.on('end', () => resolve({statusCode, contentType, rawData}));
        }).on('error', e => console.log(e));
    })
}

const getPageHTML = ClientFunction(() => {
    return document.documentElement.outerHTML;
});

const getPageLocation = ClientFunction(() => {
    return document.location.href;
});

const fetchURL = ClientFunction(url => {
    return fetch(url)
})

const elementWithClass = Selector(cn => {
    return document.getElementsByClassName(cn);
})


const printPageHTML = async () => {
    console.log(await getPageHTML());
}

const printPageLocation = async (prefix) => {
    console.log(prefix + ":" + (await getPageLocation()));
};

// this is probably only required in the short term, until the new UI hits master?
const setFormTargetSelf = ClientFunction(() => {
    document.getElementById('pg_form').target = "_self";
});

fixture`DALiuGE Start Local Managers`
    .page`http://${DALIUGE_ENGINE_IP}:${DALIUGE_NODE_PORT}`

test('start', async t => {
    await t.setNativeDialogHandler(() => true);

    await startLocalManagers();
}).timeouts(TIMEOUTS_OPTIONS);

fixture`Test Translator`
    .page`http://${DALIUGE_TRANSLATOR_IP}:${DALIUGE_TRANSLATOR_PORT}/gen_pgt`

test('is running', async t => {

    await t.setNativeDialogHandler(() => true);

    //await printPageLocation("Test Translator");
    await t.expect(Selector("body").innerText).contains("logical graph None not found", {timeout: 15000});
}).timeouts(TIMEOUTS_OPTIONS);

fixture`Test Data Island Manager`
    .page`http://${DALIUGE_ENGINE_IP}:${DALIUGE_DIM_PORT}/`

test('is running', async t => {

    await t.setNativeDialogHandler(() => true);

    //await printPageLocation("Test DIM");
    await t.expect(Selector("nav.navbar .navbar-brand span").innerText).contains("DataIslandManager", {timeout: 15000});
}).timeouts(TIMEOUTS_OPTIONS);

fixture`Test Node Manager`
    .page`http://${DALIUGE_ENGINE_IP}:${DALIUGE_NM_PORT}/`

test('is running', async t => {

    await t.setNativeDialogHandler(() => true);

    //await printPageLocation("Test NM");
    await t.expect(Selector("nav.navbar .navbar-brand span").innerText).contains("NodeManager", {timeout: 15000});
}).timeouts(TIMEOUTS_OPTIONS);

fixture`Test EAGLE`
    .page`http://${EAGLE_IP}:${EAGLE_PORT}/`

test('is running', async t => {

    await t.setNativeDialogHandler(() => true);

    //await printPageLocation("Test EAGLE");
    await t.expect(Selector("nav.navbar #eagleAndVersion a").innerText).contains("EAGLE", {timeout: 15000});
}).timeouts(TIMEOUTS_OPTIONS);

fixture`DALiuGE Regression Test`
    .page`http://${EAGLE_IP}:${EAGLE_PORT}/`

// get list of graphs to execute from GRAPHS environment variable, store in GRAPHS global variable
for (let graph of process.env.GRAPHS.split("\n")) {
    GRAPHS.push(graph);
}
console.log("GRAPHS:\n", GRAPHS);

REPRO_LOC = process.env.REPROLOC;
console.log("REPRO_LOC:\n", REPRO_LOC);

for (let i = 0; i < GRAPHS.length; i++) {
    let graphUrl = GRAPHS[i];
    console.log("GRAPH: ", graphUrl);
    test('Load-Translate-Execute: ' + graphUrl, async t => {

        // wait before starting, so give everything an extra chance to be ready
        await t.wait(SHORT_WAIT_DURATION);

        // load graph from filesystem
        await loadGraph(graphUrl);
        console.log("Loaded graph internally");
        // !!!!!!!!!!!! Load old reprodata and compare
        console.log("Fetching Local Reprodata")
        let filename = REPRO_LOC + graphUrl.substring(graphUrl.lastIndexOf("/") + 1, graphUrl.lastIndexOf('.graph')) + '/reprodata.out';
        console.log(filename);
        await loadReprodata(filename);
        // !!!!!!!!!!!!! SETUP
        // set a handler for the beforeunload event that occurs when the user navigates away from the page
        await t.setNativeDialogHandler(() => true);

        await t
            // wait for the page to settle down
            .wait(PAGE_TRANSITION_WAIT_DURATION)

            // click the help nav bar menu
            .click('#navbarDropdownHelp')

            // wait for the menu to open
            .wait(SHORT_WAIT_DURATION)

            // click the settings menu item
            .click("#settings")

            // wait for the settings modal to open
            .wait(SHORT_WAIT_DURATION)

            // disable the 'confirm discard changes' setting
            .click('#settingConfirmDiscardChangesButton')

            // disable the 'spawn translation tab' setting
            .click('#settingSpawnTranslationTabButton')

            // use the complex translator options
            .click('#settingUseSimplifiedTranslatorOptionsButton')

            //switch to external services tab
            .click("#settingCategoryExternalServices")

            // enter the translator url
            .typeText(Selector('#settingTranslatorURLValue'), "http://" + DALIUGE_TRANSLATOR_IP + ":" + DALIUGE_TRANSLATOR_PORT + DALIUGE_TRANSLATOR_URL, {replace: true})

            // close settings modal
            .click('#settingsModalAffirmativeButton')

        console.log("Set translator URL");
        // !!!!!!!!!!!!! LOAD GRAPH
        await t
            .click(Selector('#navbarDropdownGraph'))
            .hover(Selector('#navbarDropdownGraphNew'))
            .hover(Selector('#createNewGraph'))  // we have to make sure to move horizontally first, so that the menu doesn't disappear
            .click(Selector('#createNewGraphFromJson'))

            .typeText(Selector('#inputTextModalInput'), graphJSON, {replace: true, paste: true})

            .click('#inputTextModal .modal-footer button')

            .wait(PAGE_TRANSITION_WAIT_DURATION);

        console.log("Loaded graph");

        // !!!!!!!!!!!!! SET RMODE

        await t
            .click("#rightWindowModeTranslation")
            .wait(SHORT_WAIT_DURATION)

        const rmodeSelector = Selector("#alg1-rmode-options");
        const rmodeOption = rmodeSelector.find("option");

        await t
            // Open rmode selector
            .click(rmodeSelector)
            .click(rmodeOption.withText("ALL"));

        console.log("Clicked RMODE");
        // !!!!!!!!!!!!! TRANSLATE GRAPH
        await t

            // open the translation tab in the right window
            .click('#rightWindowModeTranslation')

            // open algorithm 1
            .click('#headingTwo')

            // click the 'Generate PGT' button
            .click('#alg1PGT');

        console.log("Hit translate");

        // wait for the graph to translate
        await t.wait(PAGE_TRANSITION_WAIT_DURATION);
        console.log("Translated the graph");
        // debug
        //await printPageLocation("Translator");

        // !!!!!!!!!!!!! DEPLOY AND EXECUTE
        // open settings modal
        await t
            .click('li.nav-item a[data-target="#settingsModal"]');
        console.log("Clicked the settings modal");
        // wait for modal to open
        await t.wait(SHORT_WAIT_DURATION);


        const DeployManager = elementWithClass('deployMethodUrl');
        // write manager host and port
        await t
            .typeText(DeployManager, "http://" + DALIUGE_ENGINE_IP + ":" + DALIUGE_DIM_PORT, {replace: true});


        // close modal (save settings)
        const SaveSettingButton = Selector('button').withAttribute('onclick', 'saveSettings()');
        await t.click(SaveSettingButton);

        // wait for modal to close
        await t.wait(SHORT_WAIT_DURATION);


        // modify the pg_form to make it open in the same tab when the button is clicked
        await setFormTargetSelf();

        // click 'generate and deploy physical graph' button
        await t.click('#activeDeployMethodButton');

        // short wait for deploy button processing
        await t.wait(SHORT_WAIT_DURATION);

        console.log("Deployed");

        // !!!!!!!!!!!!! AWAIT RESULTS
        await t
            .wait(PAGE_TRANSITION_WAIT_DURATION);

        const graphViewButton = Selector('button').withAttribute('data-text', 'View Session As Graph');
        await t.click(graphViewButton);

        // debug
        //await printPageLocation("Engine");
        //await printPageHTML();

        // check that the result is OK (within 5 mins)
        await t
            .expect(Selector("#session-status").innerText).contains(SUCCESS_MESSAGE, {timeout: 400000});

        // !!!!!!!!!!!!! Extract reproducibility data
        let sessionUrl = await getPageLocation();
        let sessionID = sessionUrl.substring(sessionUrl.indexOf("=") + 1, sessionUrl.indexOf("&view=graph"))

        let sessionReprodataUrl = "http://" + DALIUGE_ENGINE_IP + ":" + DALIUGE_DIM_PORT + "/api/sessions/" + sessionID + "/repro/data"
        console.log(sessionReprodataUrl);
        let new_reprodata;
        const repro_response = await fetchReprodata(sessionReprodataUrl);
        new_reprodata = JSON.parse(repro_response.rawData)
        console.log(new_reprodata['graph']['reprodata'])
        console.log("Reference Reprodata:")
        console.log(graphReprodata[1])
        await t.expect(graphReprodata).notTypeOf('null', 'Original Reprodata is not null');
        await t.expect(new_reprodata).notTypeOf('null', 'New Reprodata is not null');
        await t.expect(new_reprodata['graph']['reprodata']).eql(graphReprodata[1], 'Reprodata should match')

    }).timeouts(TIMEOUTS_OPTIONS);
}
